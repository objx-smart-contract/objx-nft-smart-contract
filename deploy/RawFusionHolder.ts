import { DeployFunction } from "hardhat-deploy/types";

const func: DeployFunction = async ({
  deployments: { deploy },
  getNamedAccounts,
}) => {
  const { deployer } = await getNamedAccounts();

  await deploy("RawFusionHolder", {
    skipIfAlreadyDeployed: true,
    from: deployer,
    log: true,
  });
};

func.tags = ["RawFusionHolder"];

export default func;
