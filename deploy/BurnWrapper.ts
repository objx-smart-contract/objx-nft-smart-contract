import { DeployFunction } from "hardhat-deploy/types";

const func: DeployFunction = async ({
  deployments: { deploy },
  getNamedAccounts,
}) => {
  const { deployer } = await getNamedAccounts();
  const rawFusionHolder = "0x5870b18AebdEAf70337c34f7A7D86C9a1f00a992";

  await deploy("ERC721BurnWrapper", {
    skipIfAlreadyDeployed: true,
    from: deployer,
    log: true,
    args: [rawFusionHolder],
  });
};

func.tags = ["BurnWrapper"];

export default func;
