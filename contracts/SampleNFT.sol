pragma solidity 0.8.4;

import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "erc721a/contracts/ERC721A.sol";
import "./interfaces/IRoyaltyRegistry.sol";

// @author DeDe
contract SampleNFT is ERC721A {
    mapping(uint256 => string) public tokenURIs;

    constructor(
        string memory _name,
        string memory _symbol
    ) ERC721A(_name, _symbol) {}

    function _setTokenURI(uint256 _tokenId, string memory _tokenURI) private {
      tokenURIs[_tokenId] = _tokenURI;
    }

    function mint(
        address _to,
        string[] memory _uris,
        uint256 _totalMint
    ) external{
        uint256 _totalSupply = totalSupply();

      // mint a token using erc721a
        _safeMint(_to, _totalMint);

        // set token uri
        for (uint256 i = 0; i < _uris.length; i++) {
            _setTokenURI(_totalSupply + i, _uris[i]);
        }
    }

    function mintSingle(
        address _to,
        string memory _uri
    ) external{
        uint256 _totalSupply = totalSupply();

      // mint a token using erc721a
        _safeMint(_to, 1);

        // set token uri
        _setTokenURI(_totalSupply + 1, _uri);
    }

    function tokenURI(uint256 _tokenId) public view virtual override returns (string memory) {
      return tokenURIs[_tokenId];
    }

    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC721A) returns (bool) {
        return super.supportsInterface(interfaceId);
    }
}